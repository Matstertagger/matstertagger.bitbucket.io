/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Matthew Tagupa's ME 405 Labs and Term Project", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Motor Driver", "index.html#sec_mot", null ],
    [ "Encoder", "index.html#sec_enc", null ],
    [ "Closed-Loop Proportional Controller", "index.html#sec_prop", null ],
    [ "Step Responses for Different Proportional Gains", "page_prop.html", null ],
    [ "Inertial Measurement Unit (IMU)", "page_imu.html", null ],
    [ "ME 405 Term Project Proposal: \"Do Nothing Box\"", "page_Proposal.html", [
      [ "ME 405 Term Project Proposal: The Do Nothing Box", "page_Proposal.html#sec_title", null ],
      [ "Problem Statement", "page_Proposal.html#sec_problem", null ],
      [ "Bill of Materials:", "page_Proposal.html#sec_materials", null ],
      [ "Assembly Plan:", "page_Proposal.html#sec_plan", null ],
      [ "Safety Assessment:", "page_Proposal.html#sec_safety", null ],
      [ "General Timeline:", "page_Proposal.html#sec_timeline", null ]
    ] ],
    [ "ME 405 Term Project: \"Do Nothing Box\"", "page_TermProject.html", [
      [ "Abstract", "page_TermProject.html#sec_abstract", null ],
      [ "Coding", "page_TermProject.html#sec_coding", null ],
      [ "The Box", "page_TermProject.html#sec_shell", null ],
      [ "Testing", "page_TermProject.html#sec_testing", null ],
      [ "Final Thoughts", "page_TermProject.html#sec_conclusion", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';